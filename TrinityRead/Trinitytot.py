from scrapy.selector import HtmlXPathSelector
from scrapy.spiders import Spider
from scrapy.http import Request

trinityorgan = open("TrinityDeptID.txt", "r")
DOMAIN = 'scholars.duke.edu/display/'
URLlist = []
for line in trinityorgan:
    organid = line.strip().split()[0]
    URL = 'http://' + DOMAIN + organid
    URLlist.append(URL)
trinityorgan.close()

class MySpider(Spider):
    name = DOMAIN
    allowed_domains = [DOMAIN]
    start_urls = URLlist

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        currurl = hxs.xpath('//link[@rel="alternate"]/@href').extract()
        print currurl
        URL = currurl[0].strip().split("/")[2]
        for url in hxs.xpath('//a/@href').extract():
            print "http://" + DOMAIN + URL + url
