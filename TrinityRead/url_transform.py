import fileinput

webidfile = open("TrinityProfs.txt", "w")
existorg = []

for line in fileinput.input():
    linesplit = line.strip().split("/")
    if (linesplit.count("display") == 2):
        organid = linesplit[-3]
        webid = linesplit[-1].strip()
        if (webid[3].isdigit() and (webid[:3] == "per")):
            webidfile.write(webid + "   " + organid + "\n")

webidfile.close()
fileinput.close()
