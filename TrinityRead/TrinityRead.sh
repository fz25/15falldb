# first, we need to read in all the raw info of each organization
# all the organization IDs are recorded manually in TrinityOrganID.txt
scrapy runspider Trinitytot.py > trirawurl.txt

# then we use url_tranform to pick out the info for each professor
# his unique webid + organization number
python url_transform.py trirawurl.txt

# then we need to clean up useless & debug files
rm -f *.pyc
