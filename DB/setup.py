# python program to read in data, put into classes, and build up the database
# approximately 3 steps:
# 1. open files in this directory
# 2. read data and put them into a class, put classes into dictionary
# 3. loop through dictionary to build up the database

# s1. define tructure of class "profs"
class profs():
    def __init__(self, webid):
        self.webid = webid
        self.name = ""
        self.dep = ""
        self.coid = []
        self.codp = []
    def add_name(self, infoname):
        self.name = infoname
    def add_dep(self, infodep):
        self.dep = infodep
    def add_coauth(self, coauth):
        self.coid.append(coauth)
    def add_codept(self, value):
        self.codp.append(value)

# 2. read data and put into class
# firstly, we read publications.txt
infofile = open("publications.txt", "r")
profsdict = {}
print "opening file: publications.txt ... "

lineindex = 0
currid = " "
currname = " "
currdep = " "
for line in infofile:
    lineindex += 1
    if line.startswith("ID:"):
        if lineindex != 2:
            newprof = profs(currid)
            newprof.add_name(currname)
            newprof.add_dep(currdep)
            profsdict[currid] = newprof
        currid = line.split(":")[1].strip()
        # print "id = " + currid
    if line.startswith("NAME:"):
        currname = line.split(":")[1].strip()
    if line.startswith("DEP:"):
        currdep = line.split(":")[1].strip()
newprof = profs(currid)
newprof.add_name(currname)
newprof.add_dep(currdep)
profsdict[currid] = newprof
infofile.close()

# to check whether pointers are correct
for ids in profsdict.keys():
    print "id = " + ids + ", name = " + profsdict[ids].name + ", dep = " + profsdict[ids].dep

# secondly, we read coauthnet.txt
infofile2 = open("coauthnet.txt", "r")
currprofid = " "
for line in infofile2:
    if line.startswith("  id:"):
        currprofid = line.strip().split("/")[-1].strip()
    if line.startswith("  co:"):
        coauth = line.strip().split(":")[1]
        profsdict[currprofid].add_coauth(coauth)
infofile2.close()

# still, we need to check whether it's correct
for ids in profsdict.keys():
    print "id = " + ids + ", coauthlist = " + str(len(profsdict[ids].coid))

# Thirdly, we can loop through each professor to calculate his related departments
infofile3 = open("TrinityProfs.txt", "r")
proflink = {}
for line in infofile3:
    thisline = line.strip().split()
    currprofid = thisline[0]
    currprofdep = thisline[1]
    proflink[currprofid] = currprofdep
infofile3.close()
infofile4 = open("TrinityDeptID.txt", "r")
deptname = {}
for line in infofile4:
    thisline = line.strip().split()
    if len(thisline) == 0: continue
    deptid = thisline[0]
    deptna = " ".join(thisline[1:])
    deptname[deptid] = deptna
infofile4.close()

for ids in profsdict.keys():
    thisprof = profsdict[ids]
    dept = {}
    totweight = 0
    for coauth in thisprof.coid:
        coauthid = coauth.split()[0]
        weight = int(coauth.split()[1])
        if coauthid in proflink.keys():
            totweight += weight
            codept = proflink[coauthid]
            if codept in dept.keys():
                dept[codept] += weight
            else:
                dept[codept] = weight
    for codept in dept.keys():
        codeptname = deptname[codept]
        thisprof.add_codept(codeptname + "   " + str(dept[codept]*1.0/totweight))

# still, we need to check correctness
for ids in profsdict.keys():
    if len(profsdict[ids].codp) == 0: continue
    print "id = " + ids + ", first dept related = " + profsdict[ids].codp[0]

# 3. loop through dictionary to print xml
# for structs in dictionary:
# id = ..., name = ..., pub = ...
# outfile.write(...)

outfile = open("rawdata.xml", "w")
outfile.write("<?xml version=\"1.0\" encoding=\"utf-8\"?> \n")
outfile.write("<duke> \n")
outfile.write("<professors> \n")
for ids in profsdict.keys():
    thisprof = profsdict[ids]
    if len(thisprof.codp) == 0:
        outfile.write("    <person id = \"" + ids + "\" name = \"" + thisprof.name + "\" dept = \"" + thisprof.dep + "\" /> \n")
    else:
        outfile.write("    <person id = \"" + ids + "\" name = \"" + thisprof.name + "\" dept = \"" + thisprof.dep + "\" > \n")
        for codept in thisprof.codp:
            codeptname = " ".join(codept.split()[:-1])
            codeptweight = int(float(codept.split()[-1]) * 100)
            outfile.write("        <codept name = \"" + codeptname + "\" weight = \"" + str(codeptweight) + "%\" /> \n")
        outfile.write('''    </person> \n''')

outfile.write('''</professors> \n''')
outfile.write("</duke>")
outfile.close()







