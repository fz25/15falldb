from scrapy.selector import HtmlXPathSelector
from scrapy.spiders import Spider
from scrapy.http import Request

DOMAIN = 'scholars.duke.edu/visualizationData?vis=coauthorship&uri=https%3A%2F%2Fscholars.duke.edu%2Findividual%2F'
URL = "http://" + DOMAIN
infofile = open("WebIDs.txt", "r")
urllist = []
for line in infofile:
    urllist.append(URL + line.split()[0] + "&vis_mode=coauthors.csv")
    # print "  url: > " + URL + line.split()[0] + "&vis_mode=coauthors.csv"
infofile.close()

# print
# print "-----------------------------------------------------"
# print "-- Below: for each professor, his full name on url --"
# print "-- and his coauthors for each paper are listed.    --"
# print "-----------------------------------------------------"

# here comes the main function
class MySpider(Spider):
    name = DOMAIN
    allowed_domains = [DOMAIN]
    start_urls = urllist

    def parse(self, response):
        # this is the main function for scarpy to grab data
        hxs = HtmlXPathSelector(response)
        coauthlist = hxs.xpath('//node').extract()
        if (len(coauthlist) <= 1):
            #print "not recorded in duke co-author network"
            print "NULL"
            print
        else:
            print hxs.xpath('//node[./@id="1"]/data[./@key="label"]/text()').extract()[0]
            print "  id:" + hxs.xpath('//node[./@id="1"]/data[./@key="url"]/text()').extract()[0]
            allco = hxs.xpath('//node[@id!="1"]')
            for coauth in allco:
                uniqueid = coauth.xpath('data[@key="url"]/text()').extract()[0]
                uniqueid = uniqueid.strip().split("/")[-1]
                times = coauth.xpath('data[@key="number_of_authored_works"]/text()').extract()[0]
                times = times.strip()
                print "  co: " + uniqueid + "   " + times
            print
