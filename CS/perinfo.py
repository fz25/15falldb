from scrapy.selector import HtmlXPathSelector
from scrapy.spiders import Spider
from scrapy.http import Request

DOMAIN = 'scholars.duke.edu/display/'
URL = 'http://%s' % DOMAIN
infofile = open("WebIDs.txt", "r")
urllist = []
# print "--  Professor's url list: --"
for line in infofile:
    if len(line.strip()) == 0: continue
    urllist.append(URL+line.split()[0])
    # print "  url: > " + URL + line.split()[0] + ", orgID: " + line.split()[1]
infofile.close()

# print
# print "-----------------------------------------------------"
# print "-- Below: for each professor: his full name on url --"
# print "-- and his coauthors for each paper are listed.    --"
# print "-----------------------------------------------------"
# print

# here comes the main function
class MySpider(Spider):
    name = DOMAIN
    allowed_domains = [DOMAIN]
    start_urls = urllist

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        # firstly, we need unique id
        id = hxs.xpath('//link[@rel="alternate"]/@href').extract()[0]
        id = id.strip().split("/")[2]
        print
        print "ID: " + id

        # we ge the name
        profname = hxs.xpath('.//title/text()').extract()[0]
        print "NAME: " + profname.strip()

        # we get the current affiliated department
        currorgan = hxs.xpath('//ul[@id="individual-personInPosition"]/*/a[@title="organization name"]/@href').extract()
        print "DEP: " + currorgan[0].split("/")[-1]

        # then we get their publications (part of) and authors related
        paperlist = hxs.xpath('//li[./@class="subclass" and ./h3/text()="Journal Articles"]/*/li[@role="listitem"]/text()').extract()
        if (len(paperlist) == 0):
            print "PAPER: EMERITUS or HAS PERSONAL PAGE or LAZY"
        for paper in paperlist:
            if (len(paper.strip())!=0 and (not paper.strip()[0].isdigit()) and (paper.strip()[0]!="(")):
                print "CO: " + paper.strip()
