from scrapy.selector import HtmlXPathSelector
from scrapy.spiders import Spider
from scrapy.http import Request

organid = "org50000500"
DOMAIN = 'scholars.duke.edu/display/'
URL = 'http://' + DOMAIN + organid

class MySpider(Spider):
    name = DOMAIN
    allowed_domains = [DOMAIN]
    start_urls = [URL]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        print URL
        for url in hxs.xpath('//a/@href').extract():
            print URL + url
