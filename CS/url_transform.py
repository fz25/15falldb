import fileinput

webidfile = open("WebIDs.txt", "w")
existid = []

for line in fileinput.input():
    linesplit = line.strip().split("/")
    if (linesplit.count("display") == 2):
        organid = linesplit[-3]
        webid = linesplit[-1].strip()
        if (webid[3].isdigit() and (webid[:3] == "per")):
            if (webid not in existid):
                webidfile.write(webid + "   " + organid + "\n")
                existid.append(webid)

webidfile.close()
fileinput.close()
