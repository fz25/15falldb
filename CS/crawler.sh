# now, for cs department, change the id of depart at rawid to org50000500
# do the following
# print out id fetched from cs webpage to urls.out
scrapy runspider rawid.py > urls.out

# now, we should take the professor's ids from urls.out
# into a form of
# perXXXXXXX   org50000500
# ...
# use >> wc -l WebIDs.txt to check the number of lines (equal to the number
# of professors in WebIDs.txt file)
python url_transform.py urls.out

# then, in order to read publications listed on that professor's personal
# webpage, we use scrapy again to read one by one, and the outputs are in
# publications.txt
scrapy runspider perinfo.py > publications.txt

# then, we can also read the co-authors from duke co-author network
# still use scrapy to read, and we'll output into coauthnet.txt
# if a prof isn't listed in duke co-author network, message will be
# printed...
scrapy runspider coauthor.py > coauthnet.txt

# to clear up debug files
rm -f *.pyc urls.out

# last step, copy all updated files to folder: DB
cp publications.txt WebIDs.txt coauthnet.txt ../DB
