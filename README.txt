------------------------------------------------------------
------------  Group Project for CS316 Database  ------------
------------------------------------------------------------

Members: Cindy, Justin, Jane, April
Duration: duke 2015 fall semester
Document: https://docs.google.com/document/d/1O8NqrjBfS-fKPSiPTydIITAI8ajKyphM4X_uZKV3neY/edit


-- File Description:
1. To run all web-crawler python programs, we have to download and 
   install scrapy.
2. Desciptions are all written in .sh files or the online Document
3. To run the files in each folder, find the only .sh file in the
   folder, and run >> ./<filename>.sh

-- Current progress: Oct 28 2015
can use crawler to take each professor's personal webpage url, name,
duke coauthors(from coauth network), coauthors(under publications 
under personal webpage)...


-- Current progress: Nov 5 2015
Added the TrinityRead folder, in this folder
1. All Trinity Professors' info are read in TrinityProfs.txt, format:
   >> prof's unique webid + organization number
2. All the Trinity department numbers are manually written to 
   TrinityOrganID.txt, format:
   >> organization number + department name
3. Execute program: ./TrinityRead.sh
Folder ends...

In the main folder, has updated some programs to perform better...
No new functions.

Current data tree:
publications.txt: profs(id, organization, coauthor of publication)
coauthnet.txt: profs(id, co-authors read from duke coauth network)
WebIDs.txt: profs(id, organization)


-- Current progress: Nov 17 2015
Have updated the DB folder
1. The generated database file is data.xml.
2. The correspondent dtd file is structure.dtd, the xml file can
   fit in dtd standard
3. Have tested: can use xquery to extract data from data.xml.
4. How to generate database: ./setup.sh

Currently, these are for professors in CS department, if implement
well, can build database for all professors in Trinity College

Future task:
Learn how to build frontend, based on our current database...

-- Current progress: Dec 1 2015
1. Have updated the website, html+css+javascript
2. Can display fundamental information but poor css
3. to do: 
   - update xml dept id to dept name
   - update profile css
   - add which dept has been chosen, how many profs colisted
   - add prof personal picture...

-- Current progress: Dec 2 2015
1. Have updated database to csdata.xml and tridata.xml, better
   formate, and won't change any longer.
2. Have found pattern of personal picture, will update later


