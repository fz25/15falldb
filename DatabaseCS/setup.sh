#!/bin/bash
# 1. use python to generate the raw xml file
python setup.py

# 2. indent the file
xmllint --format --recover rawdata.xml > data.xml

# 3. check whether it's correct
# if correct, no output warnings
xmllint --dtdvalid structure.dtd --noout data.xml
